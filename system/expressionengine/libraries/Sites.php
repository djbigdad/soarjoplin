<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * ExpressionEngine - by EllisLab
 *
 * @package		ExpressionEngine
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2003 - 2014, EllisLab, Inc.
 * @license		http://ellislab.com/expressionengine/user-guide/license.html
 * @link		http://ellislab.com
 * @since		Version 2.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * ExpressionEngine Sites Class
 *
 * @package		ExpressionEngine
 * @subpackage	Core
 * @category	Core
 * @author		EllisLab Dev Team
 * @link		http://ellislab.com
 */
class EE_Sites {

	public $num_sites_allowed = 'BlQGAgNIXCQARlYNUzcHWlt4';
	public $the_sites_allowed = 'BThcP1VDA2sKRg9CVgVVYQdl';
	public $sites_allowed_num = 'VEENYlIVUisAXAR6B2QGOQ';
}
// END CLASS

/* End of file Sites.php */
/* Location: ./system/expressionengine/libraries/Sites.php */