/*
* Uses jQuery!!!!111one
*/


/*====================================
=            ON DOM READY            =
====================================*/
$(function() {
  
    // Toggle Nav on Click
    $('.toggle-nav').click(function() {
        $(this).find('i').toggleClass('fa-close fa-bars');
        $(this).toggleClass('change');
        // Calling a function in case you want to expand upon this.
        //toggleNav();
        $('#site-menu').toggleClass('change');
    });

    $('.slider').owlCarousel({
        center: true,
        items:1,
        nav: true,
        loop:true,
        center: true,
        autoplay: 4500,
        slideSpeed: 1700,
        responsive: true,
        responsiveRefreshRate: 100,
        autoplayHoverPause: true,
        //stagePadding: 100,
        // animateOut: 'bounceOut',
        // animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        responsive:{
            600:{items:1}
        }
    });
});


/*========================================
=            CUSTOM FUNCTIONS            =
========================================*/
// function toggleNav() {
//     if ($('#site-wrapper').hasClass('show-nav')) {
//         // Do things on Nav Close
//         $('#site-wrapper').removeClass('show-nav');
//     } else {
//         // Do things on Nav Open
//         $('#site-wrapper').addClass('show-nav');
//     }

//     //$('#site-wrapper').toggleClass('show-nav');
// }